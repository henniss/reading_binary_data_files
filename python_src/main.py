#!/usr/bin/python

import os
import numpy as np
import matplotlib.pyplot as plt
from module_read_binary_data import read_binary_data as read
from module_plot_from_binary import plot_from_binary as plot

#
#==> Create graphics directory
#
graphics_path = '../graphics'
if not os.path.isdir(graphics_path):
    os.makedirs(graphics_path)

#
#==> Generate plot from Fortran data
#
path = '../fortran_src/dat/'
title = 'Visualizing Fortran generated data'
name = graphics_path + '/fortran_plot.png'
plot(path, title, name)

#
#==> Generate plot from C++ data
#
path = '../cpp_src/dat/'
title = 'Visualizing C++ generated data'
name = graphics_path + '/cpp_plot.png'
plot(path, title, name)

#
#==> Generate plot natively in Python 
# 
title = 'Visualizing natively generated data in Python'
name = graphics_path + '/python_plot.png'

def plot_natively(title, name):
    
    # Generate data
    Y, X = np.mgrid[-3:3:100j, -3:3:100j]
    U = -1 - X**2 + Y
    V = 1 + X - Y**2
    
    # Generate plots
    fig0, ax0 = plt.subplots()
    strm = ax0.streamplot(X, Y, U, V, color=U, linewidth=2, cmap=plt.cm.autumn)
    fig0.colorbar(strm.lines)
    
    plt.title(title) 
    plt.savefig(name)
    
    # Release memory
    plt.close()

plot_natively(title, name)
