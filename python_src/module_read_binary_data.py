#!/usr/bin/python

import numpy as np

def read_binary_data(file_path):
    
    # Read grid x from binary file
    x_file = open(file_path + 'x.dat', 'rb')
    x = np.fromfile(x_file, dtype=np.float64, count=-1)
    x_file.close()
    
    # Read velocity u from binary file
    u_file = open(file_path + 'u.dat', 'rb')
    u = np.fromfile(u_file, dtype=np.float64, count=-1)
    u_file.close()
    u = np.reshape(u, (np.size(x), np.size(x))) 
    
    # Read velocity v from binary file
    v_file = open(file_path + 'v.dat', 'rb')
    v = np.fromfile(v_file, dtype=np.float64, count=-1)
    v_file.close()
    v = np.reshape(v, (np.size(x), np.size(x))) 
    
    return x, u, v