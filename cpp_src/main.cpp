#include<iostream>
#include <cstdlib>
#include <fstream>

int main() {
	/*-------------------------------------------------------------------------
	 * Dictionary:
	 * ----------------------------------------------------------------------*/
	double *x, **u, **v;
	const double MESH = 6.0e-2;
	const int N = 100;
	/*-----------------------------------------------------------------------*/

	/*
	 * ==> Allocate memory
	 */
	x = new double[N];
	u = new double*[N];
	v = new double*[N];
	for (int i = 0; i < N; i++) {
		u[i] = new double[N];
		v[i] = new double[N];
	}

	/*
	 * ==> Compute grid x=[-3,3]
	 */
	for (int i = 0; i < N; i++) {
		x[i] = ((double) -3.0) + ((double) i) * MESH;
	}

	/*
	 * ==> Compute velocities
	 */
	for (int i = 0; i < N; i++) {
		for (int j = 0; j < N; j++) {
			u[i][j] = ((double) -1.0) - (x[i] * x[i]) + x[j];
			v[i][j] = ((double) 1.0) + x[i] - (x[j] * x[j]);
		}
	}

	/*
	 * ==> Write data to binary files
	 */

	// Create directory for data
	std::system("mkdir dat");

	// Write grid x
	std::ofstream x_file("./dat/x.dat", std::ios::out | std::ios::binary);
	x_file.write((char*) &x[0], N * sizeof(double));
	x_file.close();

	/* I just switched the order of these indices. Fortran seems
	   to write by columns (i.e. changing the first index first),
	   see
	   e.g. http://www.owlnet.rice.edu/~ceng303/manuals/fortran/FOR5_3.html. 
	-H 
	*/

	
	// Write velocity u
	std::ofstream u_file("./dat/u.dat", std::ios::out | std::ios::binary);
	for (int i = 0; i < N; i++) {
	  for(int j=0; j< N; j++){
	    u_file.write((char*) &u[j][i], sizeof(double));
	  }
	}
	u_file.close();

	// Write velocity v
	std::ofstream v_file("./dat/v.dat", std::ios::out | std::ios::binary);
	for (int i = 0; i < N; i++) {
	  for(int j=0; j< N; j++){
	    v_file.write((char*) &v[j][i], sizeof(double));
	  }
	}
	v_file.close();

	/*
	 * ==> Release memory
	 */
	delete x;
	delete[] u;
	delete[] v;

	return (0);
}
